#|
  This file is a part of on-lisp project.
|#

(defsystem "on-lisp"
  :version "0.0.0"
  :author "Michael Reis"
  :license "public"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "ch2"))))
  :description "Examples from the book, On Lisp, by Paul Graham"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "on-lisp-test"))))
