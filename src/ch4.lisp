
(proclaim '(inline last1 single append1 conc1 mklist))

(defun last1 (list)
  "Return the last element in a list."
  (first (last list)))

(defun single (list)
  "Is there only one element in the list?"
  (and (consp list)
       (not (rest list))))

(defun append1 (list object)
  "Append one objectect to the list."
  (append list (list object)))

(defun conc1 (list object)
  "Destructively append one objectect to the list."
  (nconc list (list object)))

(defun mklist (object)
  "Make objectect into a list, if not already a list."
  (if (listp object)
      object
      (list object)))

(defun longer (x y)
  "Return true if x is longer than y."
  ;;If both x and y are lists, measure length in parallel
  (labels ((compare (x y)
             (and (consp x)
                  (or (null y)
                      (compare (rest x) (rest y))))))
    (if (and (listp x) (listp y))
        (compare x y)
        (> (length x) (length y)))))

(defun filter (fn list)
  "Return the non-nil values returned by the fn as it applies to the list."
  (let ((acc nil))
    (dolist (x list)
      (let ((val (funcall fn x)))
        (when val (push val acc))))
    (nreverse acc)))

(defun group (source n)
  "Make sublists of length n out of source list. If not evenly divisible length, the last list will have the remainder."
  (when (zerop n) (error "Zero length."))
  (labels ((rec (source acc)
             (let ((rest (nthcdr n source)))
               (if (consp rest)
                   (rec rest (cons (subseq source 0 n) acc))
                   (nreverse (cons source acc))))))
    (if source
        (rec source nil)
        nil)))

(defun flatten (list)
  "Return a list of all the atoms from within list recursively."
  (labels ((rec (list acc)
             (cond ((null list) acc)
                   ((atom list) (cons list acc))
                   (t (rec (first list) (rec (rest list) acc))))))
    (rec list nil)))

(defun prune (test tree)
  "Return the tree that is the result of removing all leafs for which test returns true."
  (labels ((rec (tree acc)
             (cond ((null tree) (nreverse acc))
                   ((consp (first tree))
                    (rec (rest tree)
                         (cons (rec (first tree) nil) acc)))
                   (t (rec (rest tree)
                           (if (funcall test (first tree))
                               acc
                               (cons (first tree) acc)))))))
    (rec tree nil)))

(defun find2 (test list)
  "Test the elements of list, return the first element that tests true. Also return the value resulting from that test."
  (if (null list)
      nil
      (let ((val (funcall test (first list))))
        (if val
            (values (first list) val)
            (find2 test (rest list))))))

(defun before (x y list &key (test #'eql))
  "Return list from x onward if x is encountered within list before y, using test to determine equality. Note, if only x is encountered, will return true."
  (and list
       (let ((first (first list)))
         (cond ((funcall test y first) nil)
               ((funcall test x first) list)
               (t (before x y (rest list) :test test))))))

(defun after (x y list &key (test #'eql))
  "Return list from x onward if x is encountered within list after y, using test to determine equality."
  (let ((rest (before y x list :test test)))
    (and rest (member x rest :test test))))

(defun duplicate (object list &key (test #'eql))
  "If two copies of object are found within list, return the portion of the list starting with the second copy of object."
  (member object (rest (member object list :test test))
          :test test))

(defun split-if (test list)
  "Split the list where the test function returns true, and return both halves. If the test uses equality, the element that is equal to the test will be in the latter half."
  (let ((acc nil))
    (do ((src list (rest src)))
        ((or (null src) (funcall test (first src)))
         (values (nreverse acc) src))
      (push (first src) acc))))

(defun most (function list)
  "Take list and scoring function and return both element that has the highest score and the score. If multiple elements score the same, returns the first."
  (if (null list)
      (values nil nil)
      (let* ((wins (first list))
             (max (funcall function wins)))
        (dolist (object (rest list))
          (let ((score (funcall function object)))
            (when (> score max)
              (setf wins object max score))))
        (values wins max))))

(defun best (function list)
  "Apply a function that takes two inputs to all possible pairs of elements of the list, returning the element that wins the comparisons."
  (if (null list)
      nil
      (let ((wins (first list)))
        (dolist (object (rest list))
          (when (funcall function object wins)
            (setf wins object)))
        wins)))

(defun mostn (function list)
  "Take list and scoring function and return all elements that have the highest score in a list, and that score."
  (if (null list)
      (values nil nil)
      (let ((result (list (first list)))
            (max (funcall function (first list))))
        (dolist (object (rest list))
          (let ((score (funcall function object)))
            (cond ((> score max)
                   (setf max score
                         result (list object)))
                  ((= score max)
                   (push object result)))))
        (values (nreverse result) max))))

(defun map0-n (function n)
  "Apply function to a range, starting with 0 ending with n"
  (mapa-b function 0 n))

(defun map1-n (function n)
  "Apply function to a range, starting with 1 ending with n"
  (mapa-b function 1 n))

(defun mapa-b (function a b &optional (step 1))
  "Apply function to a range, starting with a ending with b, in steps of step."
  (do ((i a (+ i step))
       (result nil))
      ((> i b) (nreverse result))
    (push (funcall function i) result)))

(defun map-> (function start test-fn successor-fn)
  "Apply function to sequence starting at start, using successor-fn to step, and ending when test-fn is true."
  (do ((i start (funcall successor-fn i))
       (result nil))
      ((funcall test-fn i) (nreverse result))
    (push (funcall function i) result)))

(defun mappend (function &rest lists)
  "Append results of applying function to lists."
  (apply #'append (apply #'mapcar function lists)))

(defun mapcars (function &rest lists)
  "Mapcar multiple lists without needing to cons them together."
  (let ((result nil))
    (dolist (list lists)
      (dolist (object list)
        (push (funcall function object) result)))
    (nreverse result)))

(defun rmapcar (function &rest args)
  "Recursive mapcar that can work on trees."
  (if (some #'atom args)
      (apply function args)
      (apply #'mapcar #'(lambda (&rest args)
                          (apply #'rmapcar function args))
             args)))

















