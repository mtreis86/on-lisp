;;;; Examples from Paul Graham's On Lisp.

;;Page 29.

(defun bad-reverse (lst)
  "Destructively reverses list."
  (let* ((len (length lst))
         (ilimit (truncate (/ len 2))))
    (do ((i 0 (1+ i))
         (j (1- len) (1- j)))
        ((>= i ilimit))
      (rotatef (nth i lst) (nth j lst)))
    lst))

(defun good-reverse (lst)
  "Return the list that is the reverse of the input."
  (labels ((rev (lst acc)
             (if (null lst)
                 acc
                 (rev (rest lst) (cons (first lst) acc)))))
    (rev lst nil)))

()

