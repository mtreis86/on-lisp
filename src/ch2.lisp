;;;; Examples from Paul Graham's On Lisp.

;;Page 20.
(defun make-dbms (db)
  "Three closures in a list to make a database. One looks up the value associate with the key, the next adds a key/val, the last removes the key/val."
  (list
   ;;lookup
   #'(lambda (key)
       (rest (assoc key db)))
   ;;add
   #'(lambda (key val)
       (push (cons key val) db)
       key)
   ;;delete
   #'(lambda (key)
       (setf db (delete key db :key #'first))
       key)))

(defun lookup-dbms (key db)
  "Return the value of an entry of db associated with the key."
  (funcall (first db) key))

(defun add-dbms (key val db)
  "Add a key and value to db."
  (funcall (second db) key val))

(defun del-dbms (key db)
  (funcall (third db) key))

